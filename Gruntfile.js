module.exports = function (grunt) {
  grunt.initConfig({
    watch: {
      css: {
        files: 'stylesheets/**/*.scss',
        tasks: ['sass']
      },
      js: {
        files: 'scripts/**/*.ts',
        tasks: ['js']
      }
    },
    sass: {
      dist: {
        options: {
          sourcemap: 'none',
          style: 'compressed'
        },
        files: {
          'stylesheets/css/styles.css': 'stylesheets/master.scss'
        }
      }
    },
    clean: ['scripts/js'],
    ts: {
      default: {
        tsconfig: './scripts/tsconfig.json'
      }
    },
    browserify: {
      build: {
        files: {
          'scripts/js/bundle.js': ['scripts/js/**/*.js']
        },
        options: {
          browserifyOptions: {
            debug: true
          },          
          transform: [["babelify", { 
            "presets": ["@babel/preset-env"],
            "global": true,
            "ignore": ["/\/node_modules\/(?!app\/)/"]
           }]]
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'scripts/js/bundle.min.js': ['scripts/js/bundle.js']
        }
      }
    },
    connect: {
      server: {
        options: {
          port: 8765,
          base: '.'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.loadNpmTasks("grunt-ts");
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-contrib-connect');

  // define default task
  grunt.registerTask('js', ['clean', 'ts', 'browserify', 'uglify:dist']);
  grunt.registerTask('default', ['js', 'sass', 'connect', 'watch']);
};