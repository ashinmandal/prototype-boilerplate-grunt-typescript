# Prototype Boilerplate (Grunt + TypeScript)

A boilerplate for HTML prototypes using Grunt + TypeScript (ES6) + SASS. This project requires `ruby` and `SASS` gem to be installed.

### Mac node and ruby Installation:
- Install Homebrew: `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"` (when asked to install XCode Command Line Tools, say yes)
- `brew install node`
- `brew install rbenv ruby-build`

### Ubuntu node and ruby Installation
- `curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -`
- `sudo apt-get install -y nodejs`
- `sudo apt-get install -y rbenv`
- `git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build`

### Common Steps:
```
# Add rbenv to bash so that it loads every time you open a terminal
echo 'if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi' >> ~/.bash_profile
source ~/.bash_profile
# Replace .bash_profile with .bashrc or .zshrc accordingly
```
```
# Install Ruby
rbenv install 2.6.3
rbenv global 2.6.3
ruby -v

# If rbenv install fails on MacOS:
RUBY_CONFIGURE_OPTS="--disable-dtrace" rbenv install 2.6.3
```
```
# Install SASS gem for Mac
gem install sass

# Install ruby-sass for Ubuntu
sudo apt-get install ruby-sass
```
```
# Install grunt globally (use sudo if normal install fails)
npm install -g grunt-cli

# Clone repository and install dependencies
git clone git@gitlab.com:ashinmandal/prototype-boilerplate-grunt-typescript.git
cd prototype-boilerplate-grunt-typescript
npm install

# Run
grunt
```
